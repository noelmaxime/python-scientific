# Use the official latest python 3.8 slim image (currently 3.8.7-slim-buster)
FROM python:3.8-slim

LABEL maintainer=dirk.devriendt@3e.eu

COPY odbcinst.ini /etc/
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y --no-install-recommends \
    apt-utils \
    build-essential \
    cmake \
    cifs-utils \
    curlftpfs \
    default-libmysqlclient-dev \
    freetds-dev \
    ftp \
    g++ \
    gcc-multilib \
    gfortran \
    git-core \
    ibus \
    nano \
    nfs-common \
    libatlas-base-dev \
    libgeos-dev \
    liblapack-dev \
    libmagic-dev \
    libproj-dev \
    libudunits2-dev \
    pandoc \
    proj-bin \
    r-base \
    samba \
    tdsodbc \
    udunits-bin \
    unixodbc \
    unixodbc-dev \
    unzip \
    vim-tiny \
 && rm -rf /var/lib/apt/lists/*

# install R zoo package
RUN Rscript -e "install.packages('zoo', repos='http://cran.us.r-project.org/')"

ENV MPLBACKEND=agg

COPY pyproject.toml .
COPY requirements.txt .

# steps below will always be executed if `CACHE_DATE` is changed to a new value
ARG CACHE_DATE=2021-02-18
RUN pip install --no-cache-dir --upgrade pip \
 && pip install --no-cache-dir --upgrade Cython numexpr numpy scipy pandas matplotlib \
 && pip install --no-cache-dir --upgrade -r requirements.txt

CMD ["/bin/bash"]
ENTRYPOINT []
